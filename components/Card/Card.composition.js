import React from "react";
import Card from "./Card";

export const SimpleCard = (props) => {
  return (
    <Card onClick={() => alert("Simple Card Clicked")}>Show Alert</Card>
  );
};

export const DangerCard = (props) => {
  return (
    <Card style={{ backgroundColor: "red", color: "white" }}>Delete</Card>
  );
};
