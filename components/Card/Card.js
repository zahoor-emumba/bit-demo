import React from 'react';

function Card({children, style, ...rest}) {
    const allStyles ={
        ...style,
        minWidth: 100,
        minHeight: 100,
        textAlign: "center",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }
    return (
        <div style={allStyles} {...rest} >
            {children}
        </div>
    );
}

export default Card;